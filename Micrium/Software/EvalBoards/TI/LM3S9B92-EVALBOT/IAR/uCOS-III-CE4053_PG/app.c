/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2009-2010; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
* Filename      : app.c
* Version       : V1.00
* Programmer(s) : FUZZI
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include <includes.h>
#include <stdio.h>
#include "driverlib/timer.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include <os.h>
#include <binomial_heap.h>
#include <AVLTree.h>



/*
*********************************************************************************************************
*                                             LOCAL DEFINES
*********************************************************************************************************
*/

#define ONESECONDTICK             7000000

#define TASK1PERIOD                   10
#define TASK2PERIOD                   20


#define WORKLOAD1                     2
#define WORKLOAD2                     2


#define TIMERDIV                      (BSP_CPUClkFreq() / (CPU_INT32U)OSCfg_TickRate_Hz)




/*
*********************************************************************************************************
*                                            LOCAL VARIABLES
*********************************************************************************************************
*/

static  OS_TCB       AppTaskStartTCB;
static  CPU_STK      AppTaskStartStk[APP_TASK_START_STK_SIZE];

static  OS_TCB       AppTaskOneTCB;
static  CPU_STK      AppTaskOneStk[APP_TASK_ONE_STK_SIZE];

static  OS_TCB       AppTaskTwoTCB;
static  CPU_STK      AppTaskTwoStk[APP_TASK_TWO_STK_SIZE];

static  OS_TCB       AppTaskTCB1;
static  CPU_STK      AppTaskStk1[APP_TASK_START_STK_SIZE];
static  OS_TCB       AppTaskTCB2;
static  CPU_STK      AppTaskStk2[APP_TASK_START_STK_SIZE];
static  OS_TCB       AppTaskTCB3;
static  CPU_STK      AppTaskStk3[APP_TASK_START_STK_SIZE];
static  OS_TCB       AppTaskTCB4;
static  CPU_STK      AppTaskStk4[APP_TASK_START_STK_SIZE];
static  OS_TCB       AppTaskTCB5;
static  CPU_STK      AppTaskStk5[APP_TASK_START_STK_SIZE];

CPU_INT32U      iCnt = 0;
CPU_INT08U      Left_tgt;
CPU_INT08U      Right_tgt;
CPU_INT32U      iToken  = 0;
CPU_INT32U      iCounter= 1;
CPU_INT32U      iMove   = 10;
CPU_INT32U      measure=0;


/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void        AppRobotMotorDriveSensorEnable    ();
        void        leftLEDBlink                      ();
        void        rightLEDBlink                     ();
        void        LEDBlink                          ();
        void        moveForward                       ();
        void        moveBackward                      ();
        void        IntWheelSensor                    ();
        void        RoboTurn                          (tSide dir, CPU_INT16U seg, CPU_INT16U speed);

static  void        AppTaskStart                 (void  *p_arg);
static  void        AppTaskOne                   (void  *p_arg);
static  void        AppTaskTwo                   (void  *p_arg);
static  void        AppTask1                   (void  *p_arg);
static  void        AppTask2                   (void  *p_arg);
static  void        AppTask3                   (void  *p_arg);
static  void        AppTask4                   (void  *p_arg);
static  void        AppTask5                   (void  *p_arg);

void TaskScheduling();
void TaskRecursion();
void SwitchTask(int i);
OS_MEM *ReturnBinomialBuffer();
OS_MEM *ReturnAVLTreeBuffer();
/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Arguments   : none
*
* Returns     : none
*********************************************************************************************************
*/
OS_MEM        BinomialBuffer;
OS_MEM        AVLTreeBuffer;
BinomialNode BinomialPart[6][sizeof(BinomialNode)];
Node AVLTreePart[12][sizeof(Node)];
BinomialNode *BinomialBlkPtr;
Node *AVLTreeBlkPtr;
int TickCount=0;
int SecCount=0;
BinomialHeap recursionHeap=NULL;
AVLTree shceduleTree=NULL;
BinomialNode* MinimumRelease=NULL;
Node* HighestPriority=NULL;
int BasicPriority=2;

OS_MEM *ReturnBinomialBuffer(){
  return &BinomialBuffer;
}

OS_MEM *ReturnAVLTreeBuffer(){
  return &AVLTreeBuffer;
}

void SwitchTask(int i)
{
  switch(i)
    {
      OS_ERR      err;
      case 5:  OSRecTaskCreate((OS_TCB     *)&AppTaskTCB1,
                 (CPU_CHAR   *)"App Task 1", 
                 (OS_TASK_PTR ) AppTask1, 
                 (void       *) 0, 
                 (OS_PRIO     ) APP_TASK_ONE_PRIO, 
                 (CPU_STK    *)&AppTaskStk1[0], 
                 (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, 
                 (OS_TICK     ) 0u, 
                 (void       *)(CPU_INT32U) 1, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err);
                 break;
      case 15: OSRecTaskCreate((OS_TCB     *)&AppTaskTCB2,
                 (CPU_CHAR   *)"App Task 2", 
                 (OS_TASK_PTR ) AppTask2, 
                 (void       *) 0, 
                 (OS_PRIO     ) APP_TASK_ONE_PRIO, 
                 (CPU_STK    *)&AppTaskStk2[0], 
                 (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, 
                 (OS_TICK     ) 0u, 
                 (void       *)(CPU_INT32U) 1, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err);
               break;
      case 20: OSRecTaskCreate((OS_TCB     *)&AppTaskTCB3,
                 (CPU_CHAR   *)"App Task 3", 
                 (OS_TASK_PTR ) AppTask3, 
                 (void       *) 0, 
                 (OS_PRIO     ) APP_TASK_ONE_PRIO, 
                 (CPU_STK    *)&AppTaskStk3[0], 
                 (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, 
                 (OS_TICK     ) 0u, 
                 (void       *)(CPU_INT32U) 1, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err);
               break;
      case 35: OSRecTaskCreate((OS_TCB     *)&AppTaskTCB4,
                 (CPU_CHAR   *)"App Task 4", 
                 (OS_TASK_PTR ) AppTask4, 
                 (void       *) 0, 
                 (OS_PRIO     ) APP_TASK_ONE_PRIO, 
                 (CPU_STK    *)&AppTaskStk4[0], 
                 (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, 
                 (OS_TICK     ) 0u, 
                 (void       *)(CPU_INT32U) 1, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err);
               break;
      case 60: OSRecTaskCreate((OS_TCB     *)&AppTaskTCB5,
                 (CPU_CHAR   *)"App Task 5", 
                 (OS_TASK_PTR ) AppTask5, 
                 (void       *) 0, 
                 (OS_PRIO     ) APP_TASK_ONE_PRIO, 
                 (CPU_STK    *)&AppTaskStk5[0], 
                 (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE / 10u, 
                 (CPU_STK_SIZE) APP_TASK_ONE_STK_SIZE, 
                 (OS_MSG_QTY  ) 0u, 
                 (OS_TICK     ) 0u, 
                 (void       *)(CPU_INT32U) 1, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                 (OS_ERR     *)&err);
               break;
      default:break;
    }
}

void TaskRecursion()
{
  MinimumRelease=binomial_minimum(recursionHeap);
  if(MinimumRelease->key<SecCount)
  {
    //our team is using RM sheduling, so task with shorter period has higer priority
    switch(MinimumRelease->period)
    {
      case 5: shceduleTree=avltree_insert(shceduleTree,BasicPriority,5);break;
      case 15: shceduleTree=avltree_insert(shceduleTree,BasicPriority+1,15);break;
      case 20: shceduleTree=avltree_insert(shceduleTree,BasicPriority+2,20);break;
      case 35: shceduleTree=avltree_insert(shceduleTree,BasicPriority+3,35);break;
      case 60: shceduleTree=avltree_insert(shceduleTree,BasicPriority+4,60);break;
      default:break;
    }
    int temp=MinimumRelease->key;
    if(temp%5!=0){
      int temp2=temp%5;
      temp=temp-temp2;
    }
    recursionHeap=binomial_insert(recursionHeap,(temp+MinimumRelease->period),MinimumRelease->period);
    recursionHeap=binomial_delete(recursionHeap,MinimumRelease->key);
  }
}
int TmpPeriod;
void TaskScheduling()
{
  HighestPriority=avltree_minimum(shceduleTree);
  TmpPeriod=HighestPriority->period;
  shceduleTree=avltree_delete(shceduleTree,HighestPriority->key);
  SwitchTask(TmpPeriod);
}



void OSTimeTickHook()
{
  TickCount++;
  if(TickCount==1000){
     TickCount=0;
     SecCount++;
     TaskRecursion();
  }
}

int  main (void)
{
    OS_ERR  err;

    BSP_IntDisAll();                                            /* Disable all interrupts.                              */
    OSInit(&err);                                               /* Init uC/OS-III.                                      */
    
    OSMemCreate((OS_MEM  *)&BinomialBuffer,
                (CPU_CHAR *)"BinomialBuffer",
                (void  *)&BinomialPart[0][0],
                (OS_MEM_QTY )6,
                (OS_MEM_SIZE)sizeof(BinomialNode),
                (OS_ERR     *)&err);
    
    OSMemCreate((OS_MEM  *)&AVLTreeBuffer,
                (CPU_CHAR *)"AVLTreeBuffer",
                (void  *)&AVLTreePart[0][0],
                (OS_MEM_QTY )12,
                (OS_MEM_SIZE)sizeof(BinomialNode),
                (OS_ERR     *)&err);
    
    OSTaskCreate((OS_TCB     *)&AppTaskStartTCB,           /* Create the start task                                */
                 (CPU_CHAR   *)"App Task Start",
                 (OS_TASK_PTR ) AppTaskStart,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_START_PRIO,
                 (CPU_STK    *)&AppTaskStartStk[0],
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE / 10u,
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE,
                 (OS_MSG_QTY  ) 0u,
                 (OS_TICK     ) 0u,
                 (void       *) (CPU_INT32U) 0, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);
    
    
    OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III). */
    

}


/*
*********************************************************************************************************
*                                          STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'AppTaskStart()' by 'OSTaskCreate()'.
*
* Returns     : none
*
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

static  void  AppTaskStart (void  *p_arg)
{
    CPU_INT32U  clk_freq;
    CPU_INT32U  cnts;
    OS_ERR      err;
    
    (void)&p_arg;
    
    BSP_Init();                                                 /* Initialize BSP functions                             */
    CPU_Init();                                                 /* Initialize the uC/CPU services                       */
    clk_freq = BSP_CPUClkFreq();                                /* Determine SysTick reference freq.                    */
    
    cnts     = clk_freq / (CPU_INT32U)OSCfg_TickRate_Hz;        /* Determine nbr SysTick increments                     */
    OS_CPU_SysTickInit(cnts);                                   /* Init uC/OS periodic time src (SysTick).              */
    CPU_TS_TmrFreqSet(clk_freq);
    
    /* Enable Wheel ISR Interrupt */
    AppRobotMotorDriveSensorEnable();
    
    
    recursionHeap=binomial_insert(recursionHeap,5,5);
    recursionHeap=binomial_insert(recursionHeap,15,15);
    recursionHeap=binomial_insert(recursionHeap,20,20);
    recursionHeap=binomial_insert(recursionHeap,35,35);
    recursionHeap=binomial_insert(recursionHeap,60,60);
    
    shceduleTree=avltree_insert(shceduleTree,BasicPriority,5);
    shceduleTree=avltree_insert(shceduleTree,BasicPriority+1,15);
    shceduleTree=avltree_insert(shceduleTree,BasicPriority+2,20);
    shceduleTree=avltree_insert(shceduleTree,BasicPriority+3,35);
    shceduleTree=avltree_insert(shceduleTree,BasicPriority+4,60);
    
    TaskScheduling();
    
    while(1){
      OSTimeGet(&err);
      if(shceduleTree!=NULL)
      {
      TaskScheduling();
      }
    }
    
}


static  void  AppTask1 (void  *p_arg)
{ 
    OS_ERR      err;
    leftLEDBlink();
    OSRecTaskDel((OS_TCB *)0, &err);   

}

static  void  AppTask2 (void  *p_arg)
{ 
    OS_ERR      err;
    LEDBlink();
    OSRecTaskDel((OS_TCB *)0, &err);   

}

static  void  AppTask3 (void  *p_arg)
{ 
    OS_ERR      err;
    rightLEDBlink();
    OSRecTaskDel((OS_TCB *)0, &err);   

}
static  void  AppTask4 (void  *p_arg)
{ 
    OS_ERR      err;
    moveForward();
    OSRecTaskDel((OS_TCB *)0, &err);   

}
static  void  AppTask5 (void  *p_arg)
{ 
    OS_ERR      err;
    moveBackward();
    OSRecTaskDel((OS_TCB *)0, &err);   

}

static  void  AppTaskOne (void  *p_arg)
{ 
    OS_ERR      err;
    CPU_INT32U  k, i, j;
    /*
    if(iMove > 0)
    {
      if(iMove%2==0)
      {  
      RoboTurn(FRONT, 16, 50);
      iMove--;
      }
      else{
        RoboTurn(BACK, 16, 50);
        iMove++;
      }
    }
    
    for(k=0; k<WORKLOAD1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
        j=2*i;
      }
     }
    */

    moveForward();
    moveBackward();
    OSTaskDel((OS_TCB *)0, &err);   

}

void moveForward()
{
   CPU_INT32U  k, i, j;
   RoboTurn(FRONT, 16, 50);
   for(k=0; k<WORKLOAD1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
        j=2*i;
      }
     }
    BSP_MotorStop(LEFT_SIDE);
    BSP_MotorStop(RIGHT_SIDE);
}

void moveBackward()
{
   CPU_INT32U  k, i, j;
   RoboTurn(BACK, 16, 50);
   for(k=0; k<WORKLOAD1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
        j=2*i;
      }
     }
    BSP_MotorStop(LEFT_SIDE);
    BSP_MotorStop(RIGHT_SIDE);
}

static  void  AppTaskTwo (void  *p_arg)
{   
    OS_ERR      err;
    CPU_INT32U  i,k,j=0;
    /*
    for(i=0; i <(ONESECONDTICK); i++)
    {
      j = ((i * 2) + j);
    }
    */
    /*
    BSP_LED_Off(0u);
    for(k=0; k<5; k++)
    {
      BSP_LED_Toggle(0u);
      for(i=0; i <ONESECONDTICK/2; i++)
         j = ((i * 2)+j);
    }
    
    BSP_LED_Off(0u);
    */
    leftLEDBlink();
    rightLEDBlink();
    LEDBlink();
   OSTaskDel((OS_TCB *)0, &err);

}

void leftLEDBlink()
{
    CPU_INT32U  i,j=0;
    BSP_LED_Off(0u);
    BSP_LED_Toggle(2u);
    for(i=0; i <ONESECONDTICK/2; i++)
        j = ((i * 2)+j);
    BSP_LED_Off(0u);
}

void rightLEDBlink()
{
   CPU_INT32U  i,j=0;
    BSP_LED_Off(0u);
    BSP_LED_Toggle(1u);
    for(i=0; i <ONESECONDTICK/2; i++)
        j = ((i * 2)+j);
    BSP_LED_Off(0u);
}
void LEDBlink()
{
    CPU_INT32U  i,j=0;
    BSP_LED_Off(0u);
    BSP_LED_Toggle(0u);
    for(i=0; i <ONESECONDTICK/2; i++)
        j = ((i * 2)+j);
    BSP_LED_Off(0u);
}

static  void  AppRobotMotorDriveSensorEnable ()
{
    BSP_WheelSensorEnable();
    BSP_WheelSensorIntEnable(RIGHT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
    BSP_WheelSensorIntEnable(LEFT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
}


void IntWheelSensor()
{
	CPU_INT32U         ulStatusR_A;
	CPU_INT32U         ulStatusL_A;

	static CPU_INT08U CountL = 0;
	static CPU_INT08U CountR = 0;

	static CPU_INT08U data = 0;

	ulStatusR_A = GPIOPinIntStatus(RIGHT_IR_SENSOR_A_PORT, DEF_TRUE);
	ulStatusL_A = GPIOPinIntStatus(LEFT_IR_SENSOR_A_PORT, DEF_TRUE);

        if (ulStatusR_A & RIGHT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(RIGHT_IR_SENSOR_A_PORT, RIGHT_IR_SENSOR_A_PIN);           /* Clear interrupt.*/
          CountR = CountR + 1;
        }

        if (ulStatusL_A & LEFT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(LEFT_IR_SENSOR_A_PORT, LEFT_IR_SENSOR_A_PIN);
          CountL = CountL + 1;
        }

	if((CountL >= Left_tgt) && (CountR >= Right_tgt))
        {
          data = 0x11;
          Left_tgt = 0;
          Right_tgt = 0;
          CountL = 0;
          CountR = 0;
          //BSP_MotorStop(LEFT_SIDE);
          //BSP_MotorStop(RIGHT_SIDE);
        }
        else if(CountL >= Left_tgt)
        {
          data = 0x10;
          Left_tgt = 0;
          CountL = 0;
          //BSP_MotorStop(LEFT_SIDE);
        }
        else if(CountR >= Right_tgt)
        {
          data = 0x01;
          Right_tgt = 0;
          CountR = 0;
          //BSP_MotorStop(RIGHT_SIDE);
        }
        return;
}

void RoboTurn(tSide dir, CPU_INT16U seg, CPU_INT16U speed)
{
	Left_tgt = seg;
        Right_tgt = seg;

	BSP_MotorStop(LEFT_SIDE);
	BSP_MotorStop(RIGHT_SIDE);

        BSP_MotorSpeed(LEFT_SIDE, speed <<8u);
	BSP_MotorSpeed(RIGHT_SIDE,speed <<8u);

	switch(dir)
	{
            case FRONT :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case BACK :
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            case LEFT_SIDE :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case RIGHT_SIDE:
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            default:
                    BSP_MotorStop(LEFT_SIDE);
                    BSP_MotorStop(RIGHT_SIDE);
                    break;
	}

	return;
}
